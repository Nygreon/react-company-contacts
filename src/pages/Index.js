import React, { useContext, useState } from 'react';
import SearchBar from '../components/SearchBar';
import { ContactContext } from '../contexts/ContactContext';
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container';
import SortListComponent from '../components/SortListComponent';
import SearchAndFilter from "../components/SearchAndFilter";

/* 
    Index page 
    Default page including search,sort and contacts
*/
export default () => {
    const contacts = useContext(ContactContext);
    const [query, setQuery] = useState('');
    const [sort, setSort] = useState({ sortWith: 'first', sortBy: 'asc' });

    /* 
        Gets value searched from event, set
        query and pass to component to render
        list
    */
    const handleSearch = (e) => {
        const { target: { value } } = e;
        setQuery(value !== '' ? value : '');
    };

    /*  
        Handle sorting, takes sorting value from
        dropdown to then set state and render.
        SortWith = what to sort with(e.g first name)
        SortBy = Order(ascending / descending)
    */
    const handleSort = (e) => {
        setSort((prevSortState) => ({ ...prevSortState, sortBy: e.target.value }));
    };

    const handleSortBy = (e) => {
        setSort((prevSortState) => ({ ...prevSortState, sortWith: e.target.value }));
    };

    return (
        <Container maxWidth="lg">
            <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                margin="auto"
            >
                <SearchBar
                    handleSearch={handleSearch}
                />
                <SortListComponent
                    handleSort={handleSort}
                    handleSortBy={handleSortBy}
                    sortValue={sort.sortWith}
                    sortDirection={sort.sortBy}
                />
            </Grid>
            <SearchAndFilter
                contacts={contacts}
                query={query}
                sort={sort}
            />
        </Container>
    );
}