import React, { useContext } from 'react';
import { Redirect } from 'react-router-dom';
import { ContactContext } from '../contexts/ContactContext';
import ContactDetail from '../components/ContactDetail';

/* 
    Detail page
    Page to display contact details
*/
export default ({ match: { params: { id } } }) => {
    /* Search and get user using id from usecontext */
    const contact = useContext(ContactContext).find(item => item.login.username === id);

    if (!contact) {
        return <Redirect to="/" />
    };

    return (
        <ContactDetail contact={contact} />
    );
};
