import React from "react";
import ContactList from "./ContactList";
import _ from 'lodash';
import PropTypes from 'prop-types';


/* SearchContacts for searching functionality */
const SearchContacts = ({ contacts, query, sort }) => {
    let newContacts = contacts;
    /* Take query from searching and match to name
        could also add other variables to filter on
        depending on demand.
    */
    if (query !== '') {
        newContacts = contacts.filter(item => (
            item.name.first.includes(query.toLowerCase()) ||
            item.name.last.includes(query.toLowerCase())
        ));
    }

    if (newContacts.length === 0) {
        return (<div>No result</div>);
    }
    /* Ordering the newContacts using lodash  */
    newContacts = _.orderBy(newContacts, ['name.' + sort.sortWith], sort.sortBy);

    return (
        <ContactList
            contacts={newContacts}
        />
    );
};

SearchContacts.propTypes = {
    contacts: PropTypes.array,
    query: PropTypes.string,
    sort: PropTypes.object
};

export default SearchContacts;