import React from 'react';
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

/*  
    ContactCard that is render a 
    small card with contact name and picture
*/
const useStyles = makeStyles(({
    bigAvatar: {
        margin: 'auto',
        width: 100,
        height: 100,
        marginTop: 10,
    },
    nameText: {
        fontSize: 14,
        fontFamily: 'Roboto'
    }
}));

export default function ContactCard({ contact }) {
    const classes = useStyles();

    return (
        <Card>
            <CardActionArea>
                <Avatar
                    alt="No picture"
                    src={contact.picture.large}
                    className={classes.bigAvatar}
                />
                <CardContent>
                    <Typography className={classes.nameText}>
                        {contact.name.first.toUpperCase()}
                    </Typography>
                    <Typography className={classes.nameText}>
                        {contact.name.last.toUpperCase()}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}

ContactCard.propTypes = {
    contact: PropTypes.object
}