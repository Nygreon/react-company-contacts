import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';

/*  
    Header for application
*/
export default function HideAppBar(props) {
    const trigger = useScrollTrigger();

    return (
        <>
            <CssBaseline />
            <Slide
                appear={false}
                direction="down"
                in={!trigger}
            >
                <AppBar>
                    <Toolbar>
                        <Typography variant="h6">Company Contacts</Typography>
                    </Toolbar>
                </AppBar>
            </Slide>
            <Toolbar />
        </>
    );
}