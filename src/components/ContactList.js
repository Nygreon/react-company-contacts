import React from 'react';
import ContactCard from './ContactCard';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';

/*  
    Component to render list of contactcards
    depending on props.
*/
const ContactList = props => (
    <Grid
        spacing={5}
        container
        direction="row"
        alignItems="center"
        justify="center"
    >
        {props.contacts.map((contact) => (
            <Grid
                key={contact.email}
                item
                xs={6}
                sm={3}
                md={2
                }>
                <Link
                    style={{ textDecoration: 'none' }}
                    to={`/details/${contact.login.username}`}
                >
                    <ContactCard contact={contact} />
                </Link>
            </Grid>
        ))}
    </Grid>
);

ContactList.propTypes = {
    contacts: PropTypes.array
}
export default ContactList;