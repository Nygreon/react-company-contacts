import React from 'react';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';

/*  
Component for the search bar, takes handleSearch from
props on onChange
 */
export default function SearchBar(props) {
    return (
        <form>
            <TextField
                id="outlined-search-full-width"
                label="Search field"
                type="search"
                margin="normal"
                variant="outlined"
                onChange={props.handleSearch}
            />
        </form>
    );
}

SearchBar.propTypes = {
    handleSearch: PropTypes.func
}