import React from 'react';
import { Redirect } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import Avatar from '@material-ui/core/Avatar';
import PropTypes from 'prop-types';

/*  
    Detailpage for contact.
    Will display more data on the selected contact
*/

const useStyles = makeStyles(({
    card: {
        marginTop: 30,
        marginBottom: 30
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    avatar: {
        backgroundColor: red[500],
    },
    bigAvatar: {
        margin: 'auto',
        width: 100,
        height: 100,
    },
}));

export default function ContactDetail({ contact }) {
    const classes = useStyles();

    if (!contact) {
        return <Redirect to="/" />
    };

    return (
        <Container maxWidth="sm">
            <Card className={classes.card}>
                <CardHeader
                    title={contact.name.first + ' ' + contact.name.last}
                    subheader={contact.email}
                />
                <Avatar alt="No picture " src={contact.picture.large} className={classes.bigAvatar} />
                <CardContent>
                    <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                    />
                </CardContent>
                <CardContent>
                    <Typography
                        variant="h5"
                        gutterBottom
                    >
                        Location:
                    </Typography>
                    <Typography paragraph>
                        {contact.location.city}
                    </Typography>
                    <Typography paragraph>
                        {contact.location.street}
                    </Typography>
                    <Typography paragraph>
                        {contact.location.state}
                    </Typography>
                    <Typography paragraph>
                        {contact.location.postcode}
                    </Typography>
                    <Divider />
                    <Typography
                        variant="h5"
                        gutterBottom
                    >
                        Phone:
                    </Typography>
                    <Typography paragraph>
                        {contact.phone}
                    </Typography>
                    <Typography paragraph>
                        {contact.cell}
                    </Typography>
                    <Divider />
                </CardContent>
            </Card>
        </Container>
    );
}

ContactDetail.propTypes = {
    contact: PropTypes.object
}