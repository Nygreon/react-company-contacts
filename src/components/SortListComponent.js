import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Box from "@material-ui/core/Box";
import PropTypes from 'prop-types';

/*  
    Component to decide sorting rules.
    first or last name, ascending or descending.
*/
export default function SortListComponent(props) {
    return (
        <Box>
            <Select
                value={props.sortValue}
                onChange={props.handleSortBy}
            >
                <MenuItem value='first'>Firstname</MenuItem>
                <MenuItem value='last'>Lastname</MenuItem>
            </Select>

            <Select
                value={props.sortDirection}
                onChange={props.handleSort}
            >
                <MenuItem value='asc'>Ascending</MenuItem>
                <MenuItem value='desc'>Desending</MenuItem>
            </Select>
        </Box>
    );
}

SortListComponent.propTypes = {
    handleSort: PropTypes.func,
    handleSortBy: PropTypes.func,
    sortValue: PropTypes.string,
    sortDirection: PropTypes.string
}