import React from 'react';

/*  
     UseContext to store the data so that I 
     can collect it from detailcomponent
     and get information needed without another
     api call.   
*/
export const ContactContext = React.createContext([]);
