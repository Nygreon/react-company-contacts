import React, { Component } from 'react';
import './App.css';
import { ContactContext } from './contexts/ContactContext';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import AppHeader from './components/AppHeader';
import Index from './pages/Index';
import DetailPage from './pages/DetailPage';
import _ from 'lodash';

class App extends Component {
  state = {
    contacts: []
  };

  getContacts = async (e) => {
    const contacts = await fetch(`https://randomuser.me/api/?results=50`);

    const data = await contacts.json()
    const sorted = _.orderBy(data.results, ['name.first'], ['asc']);

    this.setState({ contacts: sorted });
  };

  componentDidMount() {
    this.getContacts();
  };

  render() {
    const {
      contacts
    } = this.state;

    return (
      <ContactContext.Provider value={contacts}>
        <Router>
          <div className="App" >
            <Link to="/">
              <AppHeader></AppHeader>
            </Link>

            <Route path="/" exact component={Index} />
            <Route path="/details/:id" exact component={DetailPage} />
          </div>
        </Router>
      </ContactContext.Provider>
    );
  };
}

export default App;
